const rickMortyApiBaseUrl = 'https://rickandmortyapi.com/api/';

const buildPortalLicense = (rickMortyObj) => {

    const wubbaLubbaDubDub = document.querySelector('.js-wubba-lubba-dub-dub');
    const characterTemplate = `
        <div class="rm-card">
            <div class="card-title-wrapper">
                PORTAL LICENSE
            </div>
            <div class="card-content-wrapper">
                <div class="card-content">
                    <img src="${rickMortyObj.image}" class="character-img"
                        alt="">
                </div>
                <div class="card-content">
                    <h2 class="character-name">${rickMortyObj.name}</h2>
                    <div class="character-info-wrapper">
                        <div class="character-column">
                            Gender : <strong>${rickMortyObj.gender}</strong>
                        </div>
                        <div class="character-column">
                            Species : <strong>${rickMortyObj.species}</strong>
                        </div>
                        <div class="character-column">
                            Status : <strong>${rickMortyObj.status}</strong>
                        </div>
                        <div class="character-column">
                            Origin : <strong>${rickMortyObj.origin.name}</strong>
                        </div>
                        <div class="character-column">
                            Location : <strong>${rickMortyObj.location.name}</strong>
                        </div>
                        <div class="character-column">
                            Amount of residents : <strong>${rickMortyObj.amountResidents}</strong>
                        </div>
                        <div class="character-column">
                            First seen in : <strong>${rickMortyObj.episode}</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;

    wubbaLubbaDubDub.insertAdjacentHTML('beforeend', characterTemplate);

}
const getEpisode = (url) => {
    const episodeRequest = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
        episodeRequest.open('GET', url, true);
        episodeRequest.setRequestHeader("Content-Type", "application/json");
        episodeRequest.onload = function (event) {
            resolve(JSON.parse(event.target.responseText));
        };
        episodeRequest.send();
    })
}

const getAmountResidents = (url) => {
    const amountResidentsRequest = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
        amountResidentsRequest.open('GET', url, true);
        amountResidentsRequest.onload = function (event) {
            resolve(JSON.parse(event.target.responseText));
        };
        amountResidentsRequest.send();
    })
}

const getRickMortyCharacters = (rickMortyApiBaseUrl) => {
    const rickMortyRequest = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
        rickMortyRequest.open('GET', rickMortyApiBaseUrl + 'character', true);
        rickMortyRequest.setRequestHeader("Content-Type", "application/json");
        rickMortyRequest.onload = function (event) {
            let rickMortyJson = JSON.parse(event.target.responseText);
            resolve(JSON.parse(event.target.responseText));
        };
        rickMortyRequest.send();
    })
}

const welcomeToDimensionCOneHundredThirtySeven = () => {
    getRickMortyCharacters(rickMortyApiBaseUrl).then(charactersResult => {

        let rickMortyCharacters = charactersResult.results;
        rickMortyCharacters.forEach(async (character, index) => {
            let urlAmountResidents = character.location.url;
            let firstSeenEpisode = character.episode[0];

            const promiseResult = await Promise.all([getAmountResidents(urlAmountResidents), getEpisode(firstSeenEpisode)]).then(res => {
                character.amountResidents = res[0].residents.length;
                character.episode = res[1].name;
                buildPortalLicense(character);
            }).catch(error => {})

        });
    })
}

window.addEventListener('DOMContentLoaded', (event) => {
    welcomeToDimensionCOneHundredThirtySeven();
})