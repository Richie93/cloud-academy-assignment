// require('load-grunt-tasks')(grunt);

module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        assets: 'assets',
        distAssets: 'dist',
        watch: {
            scripts: {
                files: [
                    "<%= assets %>/js/*.js"
                ],
                tasks: ['uglify'],
                options: {
                    livereload: true
                },
            },
            html: {
                files: [
                    "index.html"
                ],
                options: {
                    livereload: true
                },
            },
            img: {
                files: [
                    "<%= assets %>/img/**/*",
                    "<%= assets %>/img/*",
                ],
                tasks: ['clean:imgFolder', 'copy'],
                options: {
                    livereload: true
                },
            },
            scss: {
                files: [
                    "<%= assets %>/scss/*.scss"
                ],
                tasks: ['sass'],
                options: {
                    livereload: true
                },
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    '<%= distAssets %>/assets/css/main.css': 'assets/scss/main.scss'
                }
            }
        },
        uglify: {
            my_target: {
                files: [{
                    '<%= distAssets %>/assets/js/main.min.js': ["assets/js/main.js"]
                }]
            }
        },
        copy: {
            fonts: {
                cwd: '<%= assets %>/fonts/',
                src: '**/*',
                dest: '<%= distAssets %>/assets/fonts',
                expand: true,
            },
        },
        serve: {
            options: {
                port: 9000
            }
        },
        open: {
            default: {
                path: 'http://127.0.0.1:9000/index.html'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-serve');
    grunt.loadNpmTasks('grunt-open');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('start', ['copy:fonts', 'uglify', 'sass', 'open:default', 'serve']);
};